\documentclass[handouts]{beamer}
\usetheme{Copenhagen}


\def\sectiontitleframe{
  \begin{frame}
  \vfill
  \centering
  \begin{beamercolorbox}[sep=8pt,center,shadow=true,rounded=true]{title}
    \usebeamerfont{title}\insertsectionhead\par%
  \end{beamercolorbox}
  \vfill
  \end{frame}
}


<<include=FALSE>>=
library(knitr)
opts_chunk$set(
concordance=FALSE, 
comment = " ",
dev='png', 
dpi=300 
)
@



\setbeamercovered{dynamic}


\begin{document}


\author{Maxim Kovalenko}
\institute{University of Antwerp}
\title{Introduction to R}
\subtitle{Session 3}



\frame{\titlepage}


\begin{frame}[fragile]{Trip down the memory lane}

Last time, we've seen how to: 
\begin{itemize}
\item How to use \texttt{names()}, \texttt{colnames()}, and \texttt{rownames()}
\item Check for \texttt{dim()}, \texttt{ncol()}, and \texttt{nrow()}
\item How to create factors (categorical variables)
\item How to subset data: \texttt{x[,]}
\item Useful functions: \texttt{c()}, \texttt{rbind()}, \texttt{cbind()}, \texttt{which()}, \texttt{match()}, \texttt{\%in\%}, \texttt{head()}, \texttt{table()}
\end{itemize}

\end{frame}

\section{Homework}
\sectiontitleframe

\begin{frame}[fragile]{Homework}
Given a vector \texttt{x = rep(c("A","B"), 4)}, transform it to the form \texttt{rep(c("A","B"), each = 4)}, e.g., by using subsetting. The transformation should use the original vector as an argument, i.e. \texttt{y <- f(x)}, where \texttt{f() is one or several functions}. Avoid specifying indices manually (imagine \texttt{length(x)} can be large or change). \textit{Find at least two different ways to achieve the result.} Feel free to use functions we haven't seen yet. 
\small
<<>>=
(x <- rep(c("A","B"), 4)) # we start with this
rep(c("A","B"), each=4)   # we want this
@

% c(x[x == "A"], x[x == "B"])
% sort(x)
% c(x[seq(1, 7, by = 2)], x[seq(2, 8, by = 2)])
% x[c(which(x == "A"), which(x == "B"))]
% rep(x[1:2], each = 4)
\normalsize
\end{frame}

\begin{frame}[fragile]{Solution 1}
Using logical comparisons and combining: 
<<>>=
x[x == "A"]
x[x == "B"]
c(x[x == "A"], x[x == "B"])
@
\end{frame}

\begin{frame}[fragile]{Solution 2}
Using positional selection: 
<<>>=
seq(1, 7, by = 2) # vector of positions
x[seq(1, 7, by = 2)] # evaluates to x[c(1,3,5,7)]
c(x[seq(1, 7, by = 2)], x[seq(2, 8, by = 2)])
@
\end{frame}

\begin{frame}[fragile]{Solution 3}
Using \texttt{which()}: 
<<>>=
which(x == "A")
x[c(which(x == "A"), which(x == "B"))]

@
Note how \texttt{c()} is now inside square brackets. 
\end{frame}

\begin{frame}[fragile]{Solution 4}
A more elegant solution using \texttt{rep()}: 
<<>>=
x[1:2]
rep(x[1:2], each = 4)
@
\end{frame}

\begin{frame}[fragile]{Solution 5}
Most concise solution: 
<<>>=
sort(x)
@

Extra takeaway: R usually offers more than one way of solving any particular problem. 
\end{frame}

\section{Functions revisited}
\sectiontitleframe

\begin{frame}[fragile]{Overview}
\begin{itemize}
\item Scoping
\item Calling functions (supplying arguments)
\end{itemize}
<<>>=
x <- 10
x # Scoping: where R looks to convert x into 10
@

\end{frame}

\subsection{Scoping and environments}

\begin{frame}[fragile]{Environments}
\begin{itemize}
\item Everything you can access from console, exists in \textit{Global Environment}: main logical space for R to look up names of variables, functions, data frames, etc. (i.e., objects). 
\item Functions have their own environments: 
\end{itemize}
<<>>=
rm(list = ls()) # clear GE - tabula rasa
my_fun <- function() { #define a simple function
	x <- 2
	y <- 3
	return(x+y)
}
@

\end{frame}



\begin{frame}[fragile]{Environments}
<<>>=
my_fun()
x
@
Variable \texttt{x} is not found, as it exists in the environment of \texttt{my\_fun()}, not in the global environment. Therefore it is not accessible directly. 
\end{frame}


\begin{frame}[fragile]{Environments}
Moreover, you can define a different variable \texttt{x} in the global environment, and its value will not collide with the one inside \texttt{my\_fun()}: 
<<>>=
x <- 999
my_fun()
@

The function \texttt{my\_fun()} still returns expected result, despite \texttt{x} having a different value in GE. 

\end{frame}

\begin{frame}[fragile]{Environments}
Environments are hierarchically related, every environment has a \textit{parent}. Functions can look up values in parent environments: 
<<>>=
my_fun2 <- function() {
y <- 111 # define y, but not x
return(x+y) # will look up x in parent environment (GE)
}
my_fun2() # returns 999+111
@
\end{frame}

\begin{frame}[fragile]{Environments}
Function \texttt{my\_fun2()} was created inside GE, so its environment's parent is GE. Therefore it takes the value of x (=999) from GE. Both \texttt{x} exist independently one from another. 
<<>>=
my_fun() # uses its own x
my_fun2() # uses x from GE
@

Scoping ensures that variables within functions (including those created by others, e.g., from loaded libraries), do not collide with your own data objects. 
\end{frame}

\subsection{Supplying function arguments}

\begin{frame}[fragile]{Function calls}

Calling most (not all) functions requires providing arguments to them. A distinction can be made between: 

\begin{itemize}
\item \textit{Formal} arguments: property of a function itself
\item \textit{Actual} arguments: what you actually supply to the function during its call
\end{itemize}
\end{frame}

\begin{frame}[fragile]{Function calls}
For example, looking at the help file for mean:

\begin{verbatim}
## Default S3 method:
mean(x, trim = 0, na.rm = FALSE, ...)
\end{verbatim}

you see it having three named arguments: \texttt{x}, \texttt{trim}, and \texttt{na.rm}. These are formal arguments. When you run
<<>>=
mean(c(2,3))
@
the vector \texttt{c(2,3)} is an actual argument for this particular function call. 
\end{frame}

\begin{frame}[fragile]{Function calls}
During a function call, actual (supplied) arguments are mapped to formal (expected) arguments in one of the following ways: 

\begin{itemize}
\item by complete name
\item by partial name
\item by position
\end{itemize}
in this particular order. 
\end{frame}


\begin{frame}[fragile]{Example: supplying arguments}
Let's define a simple function
<<>>=
f <- function(aa, ba, bb) {
	c(aa, ba, bb)
}
@
which takes three arguments and returns a vector of those combined. 
\end{frame}


\begin{frame}[fragile]{Example: supplying arguments}
The following calls are by position: 
<<>>=
f(1,2,3) 
f(3,2,1)
@

First argument becomes \texttt{aa}, second becomes \texttt{ba}, third becomes \texttt{bb}. 

\end{frame}

\begin{frame}[fragile]{Example: supplying arguments}
The following calls use combined methods: 
<<>>=
f(1,2,aa=3) # by complete name
f(3,2,a=1)  # by partial name
@
\small
First, the named argument is assigned (\texttt{aa} in the first line or \texttt{a} in the second). Second, the remaining two arguments are assigned according to their respective positions (\texttt{ba} and \texttt{bb}). The named arguments are properly assigned, even though their position has now changed. When a name is given, position is no longer taken into account. 
\normalsize
\end{frame}

\begin{frame}[fragile]{Example: supplying arguments}
Only unambiguous argument names can be shortened: 
<<>>=
f(1,2,b=3) # matches both 'ba' and 'bb'
@
\end{frame}

\begin{frame}[fragile]{Rules of the thumb}
In most cases, you'll want to: 
\begin{itemize}
\item Use positional matching for one or two first arguments (most commonly used and known). 
\item Use complete matching for anything else to avoid confusion and make your code more readable. 
\end{itemize}
For the \texttt{mean()} function... 
\begin{verbatim}
mean(x, trim = 0, na.rm = FALSE, ...)
\end{verbatim}
<<eval=FALSE>>=
mean(1:10)
mean(1:10, trim = 0.05)
@
...are good calls. 
\end{frame}

\begin{frame}[fragile]{Rules of the thumb}
This is legitimate, but a bit of an overkill: 
<<eval=FALSE>>=
mean(x = 1:10)
@

And this is simply confusing: 
<<eval=FALSE>>=
mean(1:10, n = T)
mean(1:10, , FALSE)
mean(1:10, 0.05)
mean(, TRUE, x = c(1:10, NA))
@

\end{frame}

\subsection{Default argument values}

\begin{frame}[fragile]{Default argument values}
Often arguments have default values: 
<<>>=
f <- function(a, b=3) {
a+b
}
@

Unless \texttt{b} is explicitly supplied, it will assume the value of \texttt{3}: 
<<>>=
f(7)
f(1,1)
@

\end{frame}

\begin{frame}[fragile]{Default argument values}
Default arguments allow you to write concise statements. Recall the \texttt{mean()} function: 

\begin{verbatim}
## Default S3 method:
mean(x, trim = 0, na.rm = FALSE, ...)
\end{verbatim}

Its trim argument defaults to \texttt{0} (no trim), and \texttt{na.rm} defaults to \texttt{FALSE} (do not remove missing values). Therefore you can write
<<eval=FALSE>>=
mean(1:10)
@
without having to specify all three arguments. 
\end{frame}


\begin{frame}[fragile]{Default argument values}
You \textit{must} supply arguments that have no default values. The following does not work: 
<<>>=
mean(trim=0.05, na.rm=TRUE)
@
\end{frame}

\section{Conditions and loops}
\sectiontitleframe

\subsection{*apply() function family}

\begin{frame}[fragile]{\texttt{*apply} function family}
Very often you want to apply a certain function to each element of a vector, list, data frame row or column. This is when\texttt{ *apply()} functions come in: 
\begin{itemize}
\item \texttt{apply()}: apply function over array margins
\item \texttt{lapply()}: apply function over a list or vector (returns list)
\item \texttt{sapply()}: same, returns vector by default
\item ...
\end{itemize}
\end{frame}

\begin{frame}[fragile]{\texttt{apply()}}
\begin{verbatim}
Format: apply(X, MARGIN, FUN, ...)
\end{verbatim}
\small
<<>>=
(X <- matrix(1:10, ncol = 2))
apply(X, MARGIN = 1, sum) #1 for rows, 2 for columns
@
\normalsize
\end{frame}

\begin{frame}[fragile]{\texttt{apply()}}
Any function can be applied: 
<<>>=
apply(X, 2, sd) # standard deviation
apply(X, 2, as.character) # convert to character vector
@
\end{frame}

\begin{frame}[fragile]{\texttt{apply()}}
...including custom-defined functions: 
<<>>=
my.fun <- function(x) {x + 50} # define our own function
apply(X, 2, my.fun) # apply it to every column
@
\end{frame}

\subsection{Passing additional arguments}

\begin{frame}[fragile]{\texttt{apply()}}
Additional arguments to the function being applied can be passed: 
\small
<<>>=
my.fun <- function(x, y) {x^y + 50} # x to power y
apply(X, 2, my.fun, y = 2) # y supplied in the end
@
\normalsize
Note that \texttt{y=2} provides \texttt{y} for \texttt{my.fun()}, \texttt{apply()} just passes it on. 
\end{frame}

\begin{frame}[fragile]{\texttt{apply()}}
If you look at the help page of \texttt{apply()}: 
\begin{verbatim}
Usage
apply(X, MARGIN, FUN, ...)
\end{verbatim}

The ellipsis (\ldots) at the end of formal argument list allows to pass any additional arguments without specifying them in advance. This is useful when you need to pass arguments to a function that is nested within another function (e.g., \texttt{my.fun()} within \texttt{apply()}). 
\end{frame}


\begin{frame}[fragile]{Anonymous functions}
\small
Sometimes explicitly defining a function takes too much time, especially for one-time use. An anonymous function can be used instead: 
<<>>=
apply(X, 2, function(x) {x + 50}) 
@
Function is defined without assigning a name to it. Function body is between curly brackets, those can be omitted, if function body is a single expression (command). 
\normalsize
\end{frame}

\begin{frame}[fragile]{\texttt{lapply()}}
\texttt{lapply()} takes each element of a list and applies a function to that list, returning a list of the original length: 
<<>>=
l <- list(1:3, c(1,7,13))
lapply(l, sum)
@
\end{frame}


\begin{frame}[fragile]{\texttt{lapply()}}
Since data frames are also lists, lapply is useful to apply functions to data frame columns: 
<<>>=
my.df <- data.frame(V1 = c("1", "2", "1"), V2 = c(1,7,13))
lapply(my.df, as.numeric)
@
Here, we make sure that values are numeric in all columns. 
\end{frame}

\begin{frame}[fragile]{\texttt{lapply()}}
Often you'll want to return a data frame instead of the list: 
<<>>=
as.data.frame(lapply(my.df, as.numeric))
@
Same operation, but the result is a data frame, not simply a list. 
\end{frame}

\begin{frame}[fragile]{\texttt{sapply()}}
sapply() works as lapply(), but it tries to simplify the output to the most elementary data structure that is possible.

<<>>=
my.df <- data.frame(V1 = 1:3, V2 = c(1,7,13))
sapply(my.df, sum) # returns a vector
@
\end{frame}

\subsection{\texttt{for} loops: avoid}

\begin{frame}[fragile]{Loops are slow}

\texttt{*apply family functions are used to construct \textit{vectorized loops}}. Vectorized roughly means that you supply a single object to a function, instead of iterating over the same object one its component at a time: 

<<eval=FALSE>>=
for (i in 1:2) {
  my.df[,i] <- as.numeric(my.df[,i]) # non-vectorized
}

sapply(my.df, as.numeric) # vectorized
@

Non-vectorized loops are slow and are considered bad form.  
\end{frame}

\subsection{Conditions}

\begin{frame}[fragile]{if..else}

Code execution can be depend on a condition using \texttt{if..else}: 
\small
<<>>=
my.fun <- function(x) {
if (x == 2) {return(x+10)} else
	{return(0)} 
}
my.fun(3)
my.fun(2)
@
\normalsize
Curly brackets in lines 2 and 3 can be omitted in case of a single expression; the \texttt{else} part is optional.  
\end{frame}

\begin{frame}[fragile]{\texttt{ifelse()}}

\texttt{if..else()} make a \textit{single} condition check: it is not vectorized. For vectorized comparisons, \texttt{ifelse()} function is used: 
\begin{verbatim}
ifelse(test, yes, no)
\end{verbatim}
Example: 
<<>>=
x <- 1:3
# return a when x equals 2, b otherwise
ifelse(x == 2, "a", "b") 
@
\end{frame}

\begin{frame}[fragile]{\texttt{if..else} vs. \texttt{ifelse()}}
This following code illustrates the difference between \texttt{if..else} and \texttt{ifelse()}
<<>>=
x <- 4
y <- c(8, 10, 12, 3, 17)
ifelse (x < y, x, y)
if (x < y) x else y
@

\end{frame}

\subsection{Missing values}

\begin{frame}[fragile]{Missing values}
Missing values in R are represented by \texttt{NA}. Operations on NA result in NA: 
<<>>=
x <- c(1,2,NA,1)
x+1
sum(x)
sum(x, na.rm=TRUE) # NA removed prior to sum
@
\end{frame}


\section{Practical example}
\sectiontitleframe

\begin{frame}{VBBA scale processing}

\begin{itemize}
\item VBBA: Vragenlijst Beleving en Beoordeling van de Arbeid (Questionnaire on the Experience and Evaluation
of Work)
\item 42 scales measuring various job aspects, e.g., autonomy, work pressure, social relations, etc. 
\item Generally 3-5 items per scale. Example item: \textit{Do you have problems with the work pressure?}
\item Answer categories in this example are `always', `often', `sometimes', and `never'.  
\end{itemize}

\end{frame}

\begin{frame}{VBBA scale processing}

\begin{itemize}
\item Data to be loaded from \texttt{vbba.csv}
\item Contains 3 scales, but write code as if there were all 42
\item Scores per scale calculated as follows: 
\begin{itemize}
\item Scoring from 0 to 3
\item Sum up scores per scale
\item Standardize: $\frac{raw\ score}{3*(n\ items)}*100$
\end{itemize}
\item Variable names have the following format: e.g., \texttt{Q13.2\_1}, where \texttt{Q13.2} refers to scale and \texttt{\_1} to the first item in that scale. 
\item The task is to derive the standardized score for each scale. See exercise code for further details. 
\end{itemize}

\end{frame}

\begin{frame}[fragile]{Congratulations!}

Congratulations, you are no longer a rookie. 
\vspace{0.5cm}

As of now, you are familiar with R basics and should be able to write your own code (peeking at help files or StackOverflow.com is not frowned upon). 
\vspace{0.5cm}

Great things still lie ahead: graphics with \texttt{ggplot2}; seamless, intuitive and fast data transformations with \texttt{dplyr}; and writing reproducible reports with \texttt{knitr} and \texttt{markdown}. 


\end{frame}

\section{R graphics with \texttt{ggplot2}}
\sectiontitleframe

\subsection{Introduction to ggplot2}

\begin{frame}[fragile]{Graphics in R}
There are three main graphics systems in R: 
\begin{enumerate}
\item Base plots (see this guide: \url{https://goo.gl/ncBRMt}). Minimalistic and Zen, but often awkward to code. 
\item Lattice: \url{http://www.statmethods.net/advgraphs/trellis.html}
\item ggplot2: in my view, the best system. Powerful, flexible and relatively easy to code.  
\end{enumerate}
\end{frame}

\begin{frame}[fragile]{Good sources}
\begin{itemize}
\item Chang, W. (2012). \textit{R graphics cookbook}. O'Reilly Media, Inc..
\item Wickham, H. (2016). \textit{ggplot2: Elegant graphics for data analysis}. Springer. 
\end{itemize}
PDF versions of both books can be found online with ease. The \textit{Cookbook} contains `recipes' for most common graphs, very good for beginners. Wickham's book provides general principles, based on the \textit{Grammar of Graphics} theory. The first one gives you a fish, the second teaches you how to fish, basically. 
\end{frame}

\begin{frame}[fragile]{Toy data set}
We will work with a small built-in dataset, called \texttt{mpg}, as in `miles per gallon', containing data on fuel consumption: 
\scriptsize
<<>>=
library(ggplot2) # loading the library
mpg # tibble is a more modern data.frame format from dplyr package
@
\normalsize
\end{frame}

\begin{frame}[fragile]{First graph}
\small
<<fig.height=3.8>>=
ggplot(data=mpg, aes(x = displ, y = cty)) + geom_point()
@
\normalsize
\end{frame}

\begin{frame}[fragile]{Key components}
<<eval=FALSE>>=
ggplot(data=mpg, aes(x = displ, y = cty)) + geom_point()
@
This code contains three key components every ggplot2 plot has: 
\begin{enumerate}
\item The \texttt{data} argument is self-explanatory, must be a data frame
\item \texttt{aes} defines a set of \textit{aesthetic mappings} between variables and visual properties. Engine displacement \texttt{displ} is mapped to x-dimension, and city mileage per gallon \texttt{cty} is mapped to y-dimension
\item At least one layer that describes how to render each observations. Usually it is a \textit{geom}, in this case \textit{geom\_point}.   
\end{enumerate}
\end{frame}

\subsection{Aesthetics}

\begin{frame}[fragile]{Aesthetics}
You have a number of aesthetics, aside from specifying x- and y-axes, such as:
\begin{itemize}
\item color
\item shape
\item fill 
\item size
\item ...
\end{itemize} 
For additional information run \texttt{vignette("ggplot2-specs")} command in R. 
\end{frame}

\begin{frame}[fragile]{Aesthetics: color}
\small
<<fig.height=3.5>>=
ggplot(data=mpg, aes(x = displ, y = cty, color=factor(cyl))) +
  geom_point()
@
\normalsize
\end{frame}

\begin{frame}[fragile]{Aesthetics: shape}
\small
<<fig.height=3.5>>=
ggplot(data=mpg, aes(x = displ, y = cty, shape=factor(cyl))) +
  geom_point()
@
\normalsize
\end{frame}

\begin{frame}[fragile]{Aesthetics: size}
\small
<<fig.height=3.5>>=
ggplot(data=mpg, aes(x = displ, y = cty, size=cyl)) +
  geom_point() # poor result but possible
@
\normalsize
\end{frame}

\begin{frame}[fragile]{Aesthetics vs. fixed value}
\small
<<fig.height=3.5>>=
ggplot(data=mpg, aes(x = displ, y = cty)) +
  geom_point(color="blue") # fixed value - outside aes()
@
\normalsize
\end{frame}

\subsection{Geoms}

\begin{frame}[fragile]{Geoms}
\texttt{ggplot2} has many geoms, such as: 
\begin{itemize}
\item \texttt{geom\_point()}: scatterplots
\item \texttt{geom\_smooth()}: fits a smoother to the data (very useful!)
\item \texttt{geom\_histogram()}: continuous distributions
\item \texttt{geom\_bar()}: categorical distributions
\item \texttt{geom\_boxplot()}: box-and-whisker plots
\item \texttt{geom\_text()}: text labels. 
\item ...
\end{itemize} 
For a complete list of geoms see \url{http://docs.ggplot2.org/current/index.html}. 
\end{frame}

\begin{frame}[fragile, plain]{Geoms: smooth}
\small
<<fig.height=3.8, message=FALSE>>=
# Fit a LOESS (locally weighted smoothing) line
ggplot(data=mpg, aes(x = displ, y = cty)) +
  geom_point() + geom_smooth() # combining layers
@
\normalsize
\end{frame}

\begin{frame}[fragile, plain]{Geoms: smooth}
\small
<<fig.height=3.8, message=FALSE>>=
# Fit a linear regression line
ggplot(data=mpg, aes(x = displ, y = cty)) +
  geom_point() + geom_smooth(method="lm") 
@
\normalsize
\end{frame}

\begin{frame}[fragile, plain]{Geoms: smooth}
\small
<<fig.height=3.8, message=FALSE>>=
# Linear regression with 2-nd degree polynomial
ggplot(data=mpg, aes(x = displ, y = cty)) +
  geom_point() + 
  geom_smooth(method="lm", formula=y~poly(x,2)) 
@
\normalsize
\end{frame}

\begin{frame}[fragile, plain]{Geoms: smooth}
\small
<<fig.height=3.8, message=FALSE>>=
# Combine aesthetics with geoms
ggplot(data=mpg, aes(x = displ, y = cty)) +
  geom_point() + 
  geom_smooth(method="lm", formula=y~poly(x,2),
      aes(color=factor(cyl)), se=FALSE) # remove SE intervals
@
\normalsize
\end{frame}

\begin{frame}[fragile, plain]{Geoms: histogram}
\small
<<fig.height=3.8, message=FALSE>>=
ggplot(data=mpg, aes(x = cty)) +
  geom_histogram() # histogram requires only x
@
\normalsize
\end{frame}

\begin{frame}[fragile, plain]{Geoms: histogram}
\small
<<fig.height=3.8, message=FALSE>>=
# Improving the result
ggplot(data=mpg, aes(x = cty)) +
  geom_histogram(binwidth=2) # set bar width
@
\normalsize
\end{frame}

\begin{frame}[fragile, plain]{Geoms: histogram}
\small
<<fig.height=3.6, message=FALSE>>=
# Improving the result even more
ggplot(data=mpg, aes(x = cty)) +
  geom_histogram(binwidth=2, fill="steelblue",
  	color="black") +
  theme_minimal() + # change plot theme 
  xlab("City mileage") # change x-axis label
@
\normalsize
\end{frame}

\begin{frame}{Note the workflow}
\begin{itemize}
\item You start with a basic version, and then improve step by step. 
\item Fine-tuning can take a lot of time and thought. 
\item Combining aesthetics, geoms and layers gives you a lot of options. 
\end{itemize}
\end{frame}

\begin{frame}{Homework}
Create a box-and-whiskers plot for \texttt{cty}, with a different color for each \texttt{manufacturer}. Which manufacturer offers the best fuel economy, according to that plot? 

\end{frame}

\end{document}